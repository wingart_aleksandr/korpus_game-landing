var contentSlider   = $('.content_slider_script'),
    $specSlider      = $('.specslider'),
    matchHeight     = $('[data-mh]'),
    video           = $('#video_youtube'),
    widthW          = $(window).width();

if(contentSlider.length){
  include("js/owl.carousel.js");	
}
if($specSlider.length){
  include("js/slick.js");		
}
if(matchHeight.length){
  include("js/jquery.matchHeight-min.js");
}
if(video.length){
  include("js/jquery.youtubebackground.js");
}
include("js/modernizr.js");

function include(url){ 
  document.write('<script src="'+ url + '"></script>'); 
}



$(window).load(function(){
  
  /* ------------------------------------------------
    PRELOADER START
    ------------------------------------------------ */

        var $preloader = $('#page-preloader'),
            $spinner   = $preloader.find('.preloader');
        $spinner.fadeOut();
        $preloader.delay(350).fadeOut('slow');
        
    /* ------------------------------------------------
    PRELOADER END
    ------------------------------------------------ */

  if(contentSlider.length){
    contentSlider.owlCarousel({
      navText: [ '', '' ],
    	pagination : true,
    	items : 1,
      	loop: true,
    	responsive : true,
      smartSpeed: 1000,
      nav: true
  	});
  }

  if($specSlider.length){
  // 	// function sliderResize(){
  // 		// $specSlider.owlCarousel({
	   //    navText: [ '', '' ],
	   //    pagination : true,
	   //    loop: true,
	   //    responsiveClass: true,
	   //    smartSpeed: 1000,
	   //    nav: true,
	   //    responsive:{
	   //      320:{
				// items:1,
				// center: false,
				// autoWidth:false
	   //      },
	   //      600:{
				// items:1,
				// center: false,
				// autoWidth:false
	   //      },
	   //      991:{
				// items:1,
				// center: false,
				// autoWidth:false
	   //      },
	   //      1100:{
				// items:1,
				// center: false,
				// autoWidth:false
	   //      },
	   //      1280:{
				// items:2,
				// center: true,
				// autoWidth:true
	   //      }
	   //    }
	   //  });
	    $specSlider.slick({
		  dots: true,
		  infinite: true,
		  speed: 600,
		  slidesToShow: 1,
		  centerMode: true,
		  variableWidth: true,
		  responsive: [
			{
				breakpoint: 1280,
				settings: {
					slidesToShow: 1,
					centerMode: true,
		  			variableWidth: true
				}
			},
			{
				breakpoint: 900,
				settings: {
					slidesToShow: 1,
					centerMode: false,
					variableWidth: false
				}
			},
			{
				breakpoint: 320,
				settings: {
					slidesToShow: 1,
					centerMode: false,
					variableWidth: false
				}
			}
		  ]
		});
  	// }
  	// sliderResize();


  }
  

  if(video.length){
      $('#video_youtube').YTPlayer({
        videoId: 'YrkqwYl2_Mw',
        pauseOnScroll: false,
        fitToBackground: true
      });
  }

  if($('html').hasClass('md_touch')){
    $("body").find(".geksagon_figure").addClass('hovered');
    $("body").find("#video_youtube").hide();
  }

  function bg(){
    var windowH = $(window).height();

    if($('html').hasClass('md_touch')){

      $("body").addClass('body_mobile');
      $("body").find('.bg1').addClass('mobile_bg');
    }
    else if($('html').hasClass('md_no-touch')){
      $("body").removeClass('body_mobile');
      $("body").find('.bg1').removeClass('mobile_bg');
      $("body").find('.bg1 > .first_block').css({
        'height' : windowH - 3,
        'min-height' : '500px'
      });
    }

  }
  bg();


  function heigthSlide(){
    var widthW = $(window).width();

    if(widthW <= 991 & !$("body").hasClass('mob')){
      var heightParent = $(".content_slider_big").outerHeight(),
          child        = $(".content_slider_big").find(".content_slider_item");

      $("body").addClass('mob');
      child.css({
        'height': heightParent
      });
    }

    else if(widthW > 991 & $("body").hasClass('mob')){
      $("body").removeClass('mob');
    }

  }
  heigthSlide();

  $(window).on("resize", function(){
    
    heigthSlide();
    bg();
    // setTimeout(function(){
    // 	sliderResize();
    // },200);

  });

  if($(".arrow_down").length){
    $(".arrow_down").on("click", function (event) {
      event.preventDefault();
      var id  = $(this).data('href'),
          down = $(id).offset().top;
      $('html, body').stop().animate({scrollTop: down }, 1200);
    });
  }


})